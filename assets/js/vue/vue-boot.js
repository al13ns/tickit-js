
/*** boot vue **/
Vue.use( VueResource );
Vue.use( VueTouch );
Vue.use( VueAsyncData );
Vue.use( VueRouter );
Vue.use( VueLoader );
Vue.mixin({
    methods:
    {
        $addArrayItemKeys : function( array )
        {
            var tmp = array;
            for( var key in tmp )
            {
                tmp[key]['.key'] =   key;
            }

            return tmp;
        },

        $moment :   function( dt )
        {
            return moment( dt );
            // return moment( dt ).format( 'LLL' );
        },

        $length :   function( arrayOrObject )
        {
            if( arrayOrObject instanceof Array )
                return arrayOrObject.length;
            else if( arrayOrObject instanceof Object )
                return Object.keys( arrayOrObject ).length;
            else
                return 0;
        }
    }
});
