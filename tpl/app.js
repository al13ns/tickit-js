
var app     =   {
    methods : {
        isCurrentRoute : function( route )
        {
            return this.$route.name === route;
        },
        isCurrentRouteGame : function()
        {
            if( this.isCurrentRoute( 'game' )
                || this.isCurrentRoute( 'game-create' )
                || this.isCurrentRoute( 'game-lobby' )
                || this.isCurrentRoute( 'game-play' ) )
            {
                return true;
            }
            else
                return false;
        },
        forceLogin : function()
        {
            if( !this.loggedUser() )
                router.go({ name : 'login' });
        },
        loggedUser : function()
        {
            return firebase.auth().currentUser || null;
        },
        signOut : function()
        {
            var self = this;

            firebase.auth().signOut().then(function()
            {
                // Sign-out successful.
                self.$log( 'successfully signed out' , 'warning' );

                router.go({ name : 'login' });

            }, function(error) {
                // An error happened.
            });
        },
        signIn  :   function()
        {
            router.go({ name : 'login' });
        },
        goProfile : function()
        {
            router.go({ name : 'profile' });
        }
    },
    data : function()
    {
        return {
            'firebase' :    firebase ,
            loggedUserName : null
        };
    },
    init : function()
    {
        var self = this;

        firebase.auth().onAuthStateChanged( function(user)
        {
            if (user)
            {
                self.$log( 'user loaded' );
                self.loggedUserName = user.displayName;

                //router.go({ name : 'lobby' });
            }
            else
            {
                self.$log( 'user flushed' );
                self.loggedUserName = null;

                router.go({ name : 'login' });
            }
        });
    }
};
