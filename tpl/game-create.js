


var cmp = Vue.extend(
{
    // template    :   '#comp-1-tpl' ,
    ready   :   function()
    {
        var self = this;

        //load games
    },
    data        :   function()
    {
        return {
            name    :   router.app.loggedUser().displayName + "'s game",
            width   :   15,
            height  :   15,
            toWin   :   5
        };
    },
    methods     :
    {
        createGame : function()
        {
            console.log( this.name );

            var game = db.child( 'lobby' ).push();
            var user = router.app.loggedUser();

            game.set({
                name    :   this.name,
                width   :   this.width  *1,
                height  :   this.height *1,
                toWin   :   this.toWin  *1,
                created :   firebase.database.ServerValue.TIMESTAMP
            })
            .then(function()
            {
                return game.child( 'users' ).child( user.uid ).set({
                    name    :   user.displayName ,
                    // index   :   0 ,
                    uid     :   user.uid ,
                    ready   :   false
                });
            })
            .then(function()
            {
                router.go({ name : 'game-lobby' , params : { gameId : game.key } });
            });
        }
    }
});

//poslední "ready" zahájí hru
//poslední táhnoucí hráč ukončí hru