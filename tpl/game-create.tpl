<form class="" @submit.prevent="">
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon"><label for="name">Name</label></div>
            <input type="text" v-model="name" class="form-control" id="name" placeholder="Name">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon"><label for="width">Height</label></div>
            <input type="text" v-model="width" class="form-control" id="width" placeholder="Cells">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon"><label for="height">Width</label></div>
            <input type="text" v-model="height" class="form-control" id="height" placeholder="Cells">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon"><label for="to-win">To win</label></div>
            <input type="text" v-model="toWin" class="form-control" id="to-win" placeholder="Ticks">
        </div>
    </div>
    <div class="form-group">
        <button type="submit" @click="createGame" class="btn btn-primary">Create game</button>
    </div>
</form>