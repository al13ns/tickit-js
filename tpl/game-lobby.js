
var cmp = Vue.extend(
{
    route    :  {
        activate    :   function( trans )
        {
            this.$log( 'entering game lobby' , 'info' );



            return Promise.resolve();
        }
    },
    ready   :   function()
    {
        var self    =   this;

        self.getGameRef()
        .on( 'value' , function( data )
        {
            if( !data.val() )
            {
                //game started or cancelled
                console.log( "lobby with this gameId not found: " + data.key , 'danger' );
                router.go({ name : 'game' });
            }
            else
            {
                var uid =   router.app.loggedUser().uid;

                self.$set( 'game' , data.val() );
                self.$set( 'iAmReady' , self.game.users[uid].ready );
            }
        });
    },
    data        :   function()
    {
        return {
            game    :   {
                users   :   []
            },
            iAmReady   :   false
        };
    },
    methods     :
    {
        getGameRef :   function()
        {
            return db.child( 'lobby' ).child( this.$route.params.gameId );
        },
        getUserRef  :   function()
        {
            return this.getGameRef().child( 'users' ).child( router.app.loggedUser().uid );
        },
        toggleReady :   function()
        {
            var self    =   this;
            var uid     =   router.app.loggedUser().uid;

            this.getUserRef().child( 'ready' ).set( !self.game.users[uid].ready )
            .then(function()
            {
                //find users with ready == false
                //if none, start the game

                var start   =   true;
                for( var u in self.game.users )
                {
                    if( !self.game.users[u].ready )
                    {
                        start   =   false;
                        break;
                    }
                }

                if( Object.keys( self.game.users ).length <= 1 )
                {
                    start   =   false;
                }

                if( start )
                {
                    self.startGame();
                }
            });

        },
        startGame   :   function()
        {
            console.log("starting game");

            var gamePlay        =   Object.assign({}, this.game );
            gamePlay.created    =   firebase.database.ServerValue.TIMESTAMP;
            gamePlay.current    =   0;

            var i = 0;
            for( var u in gamePlay.users )
            {
                gamePlay.users[u].index =   i++;
                gamePlay.users[u].color =   Tickit.Player.randomColor().getRGB();
            }

            var self    =   this;
            var game    =   db.child( 'play' ).child( this.$route.params.gameId );

            game.set( gamePlay )
            .then(function()
            {
                return self.getGameRef().remove();
            })
            .then(function()
            {
                router.go({ name : 'game' });
                // router.go({ name : 'game-play' , params : { gameId : game.key } });
            });
        },
        leave       :   function()
        {
            var self    =   this;

            self.getUserRef().remove()
            .then(function()
            {
                return new Promise(function(resolve,reject)
                {
                    self.getGameRef().child( 'users' )
                    .once( 'value' , function( data )
                    {
                        if( !data.val() )
                        {
                            reject();
                        }
                        else
                        {
                            resolve();
                        }
                    });
                });
            })
            .catch(function(err)
            {
                return self.getGameRef().remove();
            })
            .then(function()
            {
                router.go({ name : 'lobby' });
            });
        }
    }
});