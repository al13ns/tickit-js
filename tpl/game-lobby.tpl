<div class="grid-50 grid-parent">
    <table class="table table-condensed table-hover table-responsive">
        <tr>
            <td>name</td>
            <td>{{ game ? game.name : '' }}</td>
        </tr>
        <tr>
            <td>created</td>
            <td>
                <span>{{ game ? $moment( game.created ).fromNow() : '' }}</span>
                <span class="small">( {{ game ? $moment( game.created ).format( 'LLL' ) : '' }} )</span>
            </td>
        </tr>
        <tr>
            <td>height</td>
            <td>{{ game ? game.height : '' }}</td>
        </tr>
        <tr>
            <td>width</td>
            <td>{{ game ? game.width : '' }}</td>
        </tr>
        <tr>
            <td>toWin</td>
            <td>{{ game ? game.toWin : '' }}</td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <button type="button" class="btn btn-danger" @click.prevent="leave">Leave</button>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <button v-if="!iAmReady" v-cloak type="button" class="btn btn-success" @click.prevent="toggleReady">Ready</button>
                    <button v-if="iAmReady" v-cloak type="button" class="btn btn-warning" @click.prevent="toggleReady">Not Ready</button>
                </div>
            </td>
        </tr>
    </table>
</div>

<div class="grid-50 grid-parent">
    <table class="table table-condensed table-hover table-responsive">
        <tr v-for="user in game.users" :class="user.ready ? 'text-success' : 'text-danger'">
            <td class="text-center">{{ user.name }}</td>
            <!--<td>
                <div class="form-group">
                    <button type="button" class="btn btn-success" @click.prevent="">Votekick</button>
                </div>
            </td>-->
        </tr>
    </table>
</div>

<div class="grid-100 grid-parent">
    <div class="well grid-100 grid-parent">

    </div>
</div>