


var cmp = Vue.extend(
{
    route    :  {
        activate    :   function( trans )
        {
            this.$log( 'entering game play' , 'info' );


            return Promise.resolve();
        }
    },
    data        :   function()
    {
        return {
            game            :   null 
        };
    },
    methods     :
    {
        loggedPlayer    :   function()
        {
            var user    =   router.app.loggedUser();

            for( var i in this.game.players )
            {
                if( this.game.players[i].uid === user.uid )
                {
                    return this.game.players[i];
                }
            }

            throw new Error( "logged user is not in players arary" );
        },
        getGameRef :   function()
        {
            return db.child( 'play' ).child( this.$route.params.gameId );
        },
        getUserRef  :   function()
        {
            return this.getGameRef().child( 'users' ).child( router.app.loggedUser().uid );
        },
        makeTurn    :   function( x , y )
        {
            var player  =   this.loggedPlayer();
            var self    =   this;

            this.game.makeTurn( player , x , y )
            .then(function()
            {
                //pass turn
                return self.game.passTurn( player );
            })
            .then(function()
            {
                //save turn to db
                //save pass to db
                //transaction?

                self.getGameRef().child( 'current' ).set( self.game.currentPlayer );
                
                var newTurn    =   player.turns.slice(-1)[0];
                var newTurnIdx =   player.turns.length - 1;

                console.log( 'P' , newTurn );
                console.log( 'P' , newTurnIdx );
                
                self.getUserRef().child( 'turns' ).child( newTurnIdx ).set( newTurn );
            })
            .catch(function( err )
            {
                self.$log( err , 'danger' );
            });
        },
        resizeGrid  :   function()
        {
            var $table  =   $( "#board-grid" );

            $table.css( 'width' , '100%' );

            var cols    =   $table.find( 'tr' ).has( 'td' ).first().find( 'td' ).length;
            cols--;
            var tblW    =   $table.width();
            var colW    =   tblW / cols;

            console.log( 'cols' , cols );
            console.log( 'tblW' , tblW );
            console.log( 'colW' , colW );

            $table.find( "td" ).width( colW );
            $table.find( "td" ).height( colW );

            $table.find( "th" ).height( colW );
            $table.find( "th" ).width( colW );
        },
        styleGrid   :   function()
        {
            var $table  =   $( "#board-grid" );

            $table.find( 'tr' ).find( 'td' )
            .hover( function()
            {
                var value   =   $(this).data( 'value' );

                if( null == value )
                {
                    $(this).toggleClass( 'bg-success' );
                }
                else
                {
                    $(this).toggleClass( 'bg-danger' );
                }
            });
        },
        paintGrid   :   function()
        {
            var self    =   this;
            var $table  =   $( "#board-grid" );

            $table.find( 'tr' ).find( 'td' ).each( function()
            {
                var value   =   $(this).data( 'value' );

                if( null == value )
                {
                    return;
                }

                $(this).css( 'background-color' , self.game.players[value].color.getCSSHexadecimalRGB() );
            });
        },
        isNext      :   function( index )
        {
            if( index == this.game.getNext() )
                return true;
            else
                return false;
        },
        checkWinner :   function( gameData )
        {
            console.log( "WINNER" , this.game.checkWinners() );

            var winner  =   this.game.checkWinners();
            var self    =   this;

            if( !winner )
            {
                return null;
            }

            self.$log( "player " + winner.name + " wins!" , 'success' );

            var ref =   db.child( 'stats' ).child( self.loggedPlayer().uid );

            ref.once( 'value' ,
            function( data )
            {
                var stats   =   data.val() || { won : 0 , lost : 0 };

                if( winner === self.loggedPlayer() )
                {
                    stats.won++;
                }
                else
                {
                    stats.lost++;
                }

                ref.update( stats );
            });

            if( winner === self.loggedPlayer() )
            {
                db.child( 'history' ).child( this.$route.params.gameId ).set( gameData );
                self.getGameRef().remove(); //TODO: maybe move  to ref.update().then() ?
            }
        }
    },
    ready : function()
    {
        var self    =   this;

        //db.onDisconnect().update({ 'disconnect' : router.app.loggedUser().displayName });
        // TODO: onDisconnect remove user and re-index the others - not possible, but votekick should be

        self.getGameRef().on( 'value' ,
        function( data )
        {
            if( !data.val() )
            {
                console.log( "lobby with this gameId not found: " + data.key , 'danger' );
                router.go({ name : 'game' });
                return;
            }

            var game        =   new Tickit.Game( data.val() );

            self.$set( 'game' , game );

            setTimeout( self.styleGrid , 200 ); //TODO: i don't like this
            setTimeout( self.paintGrid , 300 ); //TODO: i don't like this

            //check winner
            self.checkWinner( data.val() );

            console.log( self.game );
        });

        console.log( 'READY' );
        
        /*
        $(document).ready( function(event) {
            console.log("DOM fully loaded and parsed");
            self.styleGrid();
        });
        */
    }
});
