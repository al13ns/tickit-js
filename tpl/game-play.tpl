<div class="grid-50 grid-parent">
    <button type="button" class="btn btn-primary" @click.prevent="resizeGrid">Fit to screen</button>
</div>

<div class="grid-50 grid-parent">
    <template v-if="game">
    <table class="table table-condensed table-hover table-responsive">
        <tr v-for="(index,player) in game.players">
            <td class="text-center">
                <span class="" :style="{ color : player.color.getCSSHexadecimalRGB() }">{{ player.name }}</span>
                <span v-if="game.currentPlayer == index" class="label label-danger">plays</span>
                <span v-if="isNext(index)" class="label label-warning">next</span>
                <span v-if="loggedPlayer() === player" class="label label-success">me</span>
            </td>
            <!--<td>
                <div class="form-group">
                    <button type="button" class="btn btn-success" @click.prevent="">Votekick</button>
                </div>
            </td>-->
        </tr>
    </table>
    </template>
</div>

<hr />

<div class="grid-100 grid-parent" style="overflow: auto;">
    <template v-if="game">
    <table id="board-grid" class="table-bordered">
        <tr>
            <th></th>
            <th v-for="i in game.board.grid[0]" track-by="$index">
                {{ $index }}
            </th>
        </tr>
        <tr v-for="(x,row) in game.board.grid" track-by="$index" v-cloak>
            <th>{{ x }}</th>
            <td v-for="(y,col) in row" track-by="$index" class="text-center" @click="makeTurn(x,y)" data-value="{{ col }}" data-x="{{ x }}" data-y="{{ y }}">
                <!--col != null ? col : '&nbsp'-->
                <!--<button style="width: 20px; height: 20px;" v-if="col === null" type="button" @click.prevent=""></button>
                <button style="width: 20px; height: 20px;" v-if="col !== null" type="button" @click.prevent="" disabled>{{ col }}</button>-->
            </td>
        </tr>
    </table>
    </template>
</div>