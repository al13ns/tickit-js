
var cmp = Vue.extend(
{
    route    :  {
        canReuse : function(trans)
        {
            return false;
        }
    },
    init    :   function()
    {
        //if there is a running game with this user, redirect to game-play
        //if there is a game-lobby with this user, redirect to game-lobby
        //else redirect to game-create
        var uid     =   router.app.loggedUser().uid;
        var self    =   this;

        return new Promise(function(resolve,reject)
        {
            db.child( 'play' )
            .orderByChild( 'users/'+ uid +'/uid' )
            .equalTo( uid )
            .once( 'value' , function( data )
            {
                if( !data.val() )
                {
                    reject();
                    return;
                }

                for( var gameId in data.val() )
                {
                    router.go({ name : 'game-play' , params : { gameId : gameId } });
                    break;
                }

                resolve();
            });
        })
        .catch(function(err)
        {
            //if there is a game-lobby with this user, redirect to game-lobby

            return new Promise(
            function( resolve , reject )
            {
                db.child( 'lobby' )
                .orderByChild( 'users/'+ uid +'/uid' )
                .equalTo( uid )
                .once( 'value' , function( data )
                {
                    if( !data.val() )
                    {
                        reject();
                        return;
                    }

                    for( var gameId in data.val() )
                    {
                        router.go({ name : 'game-lobby' , params : { gameId : gameId } });
                        break;
                    }

                    resolve();
                });
            });
        })
        .catch(function(err)
        {
            //else redirect to game-create
            router.go({ name : 'game-create' });
            return Promise.resolve();
        });

        //router.go({ name : 'game-create' });
    },
    data        :   function()
    {
        return {

        };
    },
    methods     :
    {

    }
});