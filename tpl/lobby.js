

var cmp = Vue.extend(
{
    // template    :   '#comp-1-tpl' ,
    ready   :   function()
    {
        var self = this;

        db.child( 'lobby' ).on( 'value' , function( data )
        {
            self.games  =   data.val();
            //console.log( self.games );
        });
    },
    data        :   function()
    {
        return {
            games : {}
        };
    },
    computed    :
    {

    },
    methods     :
    {
        join    :   function( gameId )
        {
            var self =  this;
            var user =  router.app.loggedUser();

            Promise.resolve()
            .then(function()
            {
                //delete all already joined games

                db.child( 'lobby' )
                .orderByChild( 'users/' + user.uid + '/uid' )
                .equalTo( user.uid )
                .once( 'value' , function( data )
                {
                    for( var gid in data.val() )
                    {
                        if( gid === gameId )
                            continue;

                        if( self.$length( data.val()[gid].users ) <= 1 )
                        {
                            //delete the game

                            db
                            .child( 'lobby' )
                            .child( gid )
                            .remove();
                        }
                        else
                        {
                            //delete the user

                            db
                            .child( 'lobby' )
                            .child( gid )
                            .child( 'users' )
                            .child( user.uid )
                            .remove();
                        }
                    }

                    return;
                });
            })
            .then(function()
            {
                //join

                return db.child( 'lobby' ).child( gameId ).child( 'users' ).child( user.uid ).set({
                    name    :   user.displayName ,
                    // index   :   self.userCnt( gameId ) ,
                    uid     :   user.uid ,
                    ready   :   false
                });
            })
            .then(function()
            {
                router.go({ name : 'game-lobby' , params : { gameId : gameId } });
            });
        },
        userCnt :   function( gameId )
        {
            var users   =   this.games[gameId].users;

            if( !users )
                return 0;

            return Object.keys( users ).length;
        }
    }
});