<table class="table table-condensed table-responsive table-hover">
    <thead>
        <tr><th>Game name</th><th>Created</th><th>Users</th><th>Join</th></tr>
    </thead>
    <tbody>
        <template v-for="(index, game) in games">
        <tr>
            <td>{{ game.name }}</td>
            <td>
                <span>{{ game ? $moment( game.created ).fromNow() : '' }}</span>
                <span class="small">( {{ game ? $moment( game.created ).format( 'LLL' ) : '' }} )</span>
            </td>
            <td>{{ userCnt( index ) }}</td>
            <td>
                <div class="form-group">
                    <button type="button" class="btn btn-success" @click.prevent="join(index)">Join</button>
                </div>
            </td>
        </tr>
        </template>
    </tbody>
</table>