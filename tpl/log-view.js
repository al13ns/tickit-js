Vue.mixin({
    methods:
    {
        $log : function( text , style , ele )
        {
            var $ele = $( ele || '.log' );

            style   =   style || 'default';
            $ele.html( $ele.html() + '<span class="text-'+style+'">' + text + '</span>' + '<br />' );
        }
    }
});

var cmp = Vue.extend(
{
    data        :   function()
    {
        return {
        };
    },
    methods     :
    {
        clearLog    :   function()
        {
            $(this.$el).parent().find('.log').empty();
        }
    }
});
