

var cmp = Vue.extend(
{
    data        :   function()
    {
        return {
            username    :   "" ,
            password    :   "" ,
            email       :   ""
        };
    },
    methods     :
    {
        login    :   function()
        {
            if( !this.email )
            {
                this.$log( 'email is empty' , 'danger' );
                return;
            }
            if( !this.password )
            {
                this.$log( 'password is empty' , 'danger' );
                return;
            }

            var self = this;

            firebase.auth()
            .signInWithEmailAndPassword( self.email , self.password )
            .then( function( user )
            {
                console.log( user );
                self.$log( 'successfully logged in' , 'success' );
                router.go({ name : 'lobby' });
            })
            .catch( function( err )
            {
                self.$log( err.message , 'danger' );
                throw err;
            });
        }
    },
    ready : function()
    {
        if( router.app.loggedUser() )
            router.go({ name : 'lobby' });
    }
});