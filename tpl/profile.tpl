<form class="" style="margin-top: 10px;" @submit.prevent="">

    <div class="form-group">
        <label for="profile-password">Current password</label>
        <input v-model="passwd" type="password" class="form-control" id="profile-password" placeholder="Your current password" />
    </div>

    <div class="form-group">
        <label for="profile-name">Display name</label>
        <div class="input-group">
            <div class="btn-group input-group-btn">
                <input type="button" class="btn btn-success" value="Update" @click.prevent="updateName" :disabled="!name || user.displayName === name" />
            </div>
            <input v-model="name" type="text" class="form-control" id="profile-name" placeholder="Your name" />
        </div>
    </div>

    <div class="form-group">
        <label for="profile-email">Email</label>
        <div class="input-group">
            <div class="btn-group input-group-btn">
                <input type="button" class="btn btn-success" value="Update" @click.prevent="updateEmail" :disabled="!email || user.email === email" />
            </div>
            <input v-model="email" type="email" class="form-control" id="profile-email" placeholder="Your email" />
        </div>
    </div>

    <div class="form-group">
        <label for="profile-photo">Photo URL</label>
        <div class="input-group">
            <div class="btn-group input-group-btn">
                <input type="button" class="btn btn-success" value="Update" @click.prevent="updatePhoto" :disabled="user.photoURL === photo" />
            </div>
            <input v-model="photo" type="url" class="form-control" id="profile-photo" placeholder="Your photo URL" />
        </div>
    </div>

    <div class="form-group">
        <label for="profile-new-password">New password</label>
        <div class="input-group">
            <input v-model="newPasswd" type="password" class="form-control" id="profile-new-password" placeholder="Your new password" />
            <div class="btn-group input-group-btn">
                <input style="border-radius: 0;" type="button" class="btn btn-success" value="Update" @click.prevent="updatePassword" :disabled="!newPasswd || newPasswd !== newPasswd2" />
            </div>
            <input v-model="newPasswd2" type="password" class="form-control" id="profile-new-password-repeat" placeholder="Your new password again" />
        </div>
    </div>

    <!--<div class="form-group">
        <label for="profile-new-password">New password</label>
        <input v-model="passwd" type="password" class="form-control" id="profile-new-password" placeholder="Your new password" />
    </div>
    <div class="form-group">
        <label for="profile-new-password">New repeat</label>
        <input v-model="passwd" type="password" class="form-control" id="profile-new-password" placeholder="Your new password" />
    </div>
    <div class="form-group">
        <div class="btn-group" role="group">
            <button type="submit" class="btn btn-success">Update profile</button>
        </div>
    </div>-->

    <div class="form-group">
        <label for="profile-uid">UID</label>
        <input :value="user.uid" readonly type="text" class="form-control" id="profile-uid" />
    </div>

    <div class="form-group">
        <label for="">Verified</label>
        <div>
            <input v-show="!user.emailVerified" type="button" class="btn btn-info" value="Send email" />
            <input v-show="!user.emailVerified" type="button" class="btn btn-warning" value="Enter code" />
            <span class="label label-success" style="font-size: 120%;"><span class="glyphicon glyphicon-ok"></span> Verified</span>
            <span class="label label-danger" style="font-size: 120%;"><span class="glyphicon glyphicon-exclamation-sign"></span> Unverified</span>
        </div>
        <!-- if false , Firebase.User.sendEmailVerification && applyActionCode -->
    </div>
</form>
