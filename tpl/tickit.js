

var Tickit  =
{
    randomInt   :   function( min , max )
    {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    Player :    class Player
    {
        constructor( userObj )
        {
            this.uid    =   userObj.uid;
            this.name   =   userObj.name;
            this.index  =   userObj.index;
            this.turns  =   userObj.turns || [];
            this.color  =   new RGBColour( userObj.color.r , userObj.color.g , userObj.color.b ) ;
        }

        static randomColor()
        {
            return new RGBColour(
                        Tickit.randomInt( 0 , 255 ) ,
                        Tickit.randomInt( 0 , 255 ) ,
                        Tickit.randomInt( 0 , 255 )
                    );
        }
    },

    Board   :   class Board
    {
        constructor( gameObj )
        {
            this.width  =   gameObj.width;
            this.height =   gameObj.height;
            this.grid   =   [];

            for( var x = 0 ; x < this.width ; x++ )
            {
                this.grid[x]   =   [];

                for( var y = 0 ; y < this.height ; y++ )
                {
                    this.grid[x][y]    =   null;
                }
            }
        }

        //get grid?

    },

    Game    :   class Game
    {
        constructor( gameData )
        {
            this.created        =   gameData.created;
            this.currentPlayer  =   gameData.current;
            this.name           =   gameData.name;
            this.toWin          =   gameData.toWin;

            this.board          =   new Tickit.Board( gameData );
            this.players        =   [];

            for( var uid in gameData.users )
            {
                this.players[gameData.users[uid].index]   =   new Tickit.Player( gameData.users[uid] )
            }

            this.mergePlayersTurns( this.players );
        }

        mergePlayerTurns( player )
        {
            for( var i in player.turns )
            {
                var x = player.turns[i].x;
                var y = player.turns[i].y;

                this.claimField( this.players.indexOf( player ) , x , y );
            }
        }

        mergePlayersTurns( players )
        {
            for( var i in players )
            {
                this.mergePlayerTurns( players[i] );
            }
        }

        claimField( player , x , y )
        {
            this.board.grid[x][y]  =   player;
        }

        makeTurn( player , x , y )
        {
            var self = this;

            return new Promise(function( resolve , reject )
            {
                if( self.board.grid[x][y] !== null )
                {
                    throw new Error( "field already taken" );
                }

                if( self.currentPlayer != self.players.indexOf( player ) )
                {
                    throw new Error( "not your turn" );
                }

                self.claimField( player , x , y );
                player.turns.push({ x : x , y : y });
                resolve();
            });
        }

        passTurn( player = null )
        {
            var self = this;

            return new Promise(function( resolve , reject )
            {
                if( player && self.currentPlayer != self.players.indexOf( player ) )
                {
                    throw new Error( "not your turn" );
                }

                self.currentPlayer  =   self.getNext();
                resolve();
            });
        }

        getNext()
        {
            var next    =   this.currentPlayer + 1;

            if( next >= this.players.length )
                next    =   0;

            return next;
        }

        checkWinners()
        {
            var winner  =   null;

            for( var i in this.players )
            {
                var player  =   this.players[i];

                if( this.checkWinner( player ) )
                {
                    winner  =   player;
                    break;
                }
            }

            return winner;
        }

        checkWinner( player )
        {
            var row     =   0;
            var col     =   0;
            var i       =   0;

            for( row = 0 ; row < this.board.width ; row++ )                       // This first for loop checks every row
            {
                for( col = 0 ; col < ( this.board.height - ( this.toWin - 1 ) ) ; col++ )           // And all columns until N away from the end
                {
                    while( this.board.grid[row][col] == this.players.indexOf( player ) )      // For consecutive rows of the current player's mark
                    {
                        col++;
                        i++;
                        if( i == this.toWin )
                        {
                            return player;
                        }
                    }
                    i = 0;
                }
            }

            for( col = 0 ; col < this.board.height ; col++ )                       // This one checks for columns of consecutive marks
            {
                for( row = 0; row < ( this.board.width - ( this.toWin - 1 ) ) ; row++ )
                {
                    while( this.board.grid[row][col] == this.players.indexOf( player ) )
                    {
                        row++;
                        i++;
                        if( i == this.toWin )
                        {
                            return player;
                        }
                    }
                    i = 0;
                }
            }

            for( col = 0; col < ( this.board.height - ( this.toWin - 1 ) ) ; col++ )             // This one checks for "forwards" diagonal runs
            {
                for( row = 0; row < ( this.board.width - ( this.toWin - 1 ) ) ; row++ )
                {
                    while( this.board.grid[row][col] == this.players.indexOf( player ) )
                    {
                        row++;
                        col++;
                        i++;
                        if( i == this.toWin )
                        {
                            return player;
                        }
                    }
                    i = 0;
                }
            }

            // Finally, the backwards diagonals:
            for( col = this.board.height - 1 ; col > 0 + ( this.toWin - 2 ) ; col-- )           // Start from the last column and go until N columns from the first
            {                                                   // The math seems strange here but the numbers work out when you trace them
                for( row = 0; row < ( this.board.width - ( this.toWin - 1 ) ) ; row++ )       // Start from the first row and go until N rows from the last
                {
                    while( this.board.grid[row][col] == this.players.indexOf( player ) )  // If the current player's character is there
                    {
                        row++;                                  // Go down a row
                        col--;                                  // And back a column
                        i++;                                    // The i variable tracks how many consecutive marks have been found
                        if( i == this.toWin )                             // Once i == N
                        {
                            return player;                      // Return the current player number to the
                        }                                       // winnner variable in the playGame function
                    }                                           // If it breaks out of the while loop, there weren't N consecutive marks
                    i = 0;                                      // So make i = 0 again
                }                                               // And go back into the for loop, incrementing the row to check from
            }

            return null;

        }
    }
};

